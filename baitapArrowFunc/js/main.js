const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
var active=''
var renderColor = () => {
  var contentColor = "";
  for (let i = 0; i < colorList.length; i++) {


    
    var content = `
<button class='color-button ${colorList[i]} '></button>
`;
    contentColor += content;
  }

  document.getElementById("colorContainer").innerHTML = contentColor;
};
renderColor();

let pickColor = document.getElementsByClassName("color-button")
  , house = document.getElementById("house");
for (let o = 0; o <  pickColor.length; o++)
     pickColor[o].addEventListener("click", function() {
        changeColor(colorList[o], o)
    });
changeColor = ((i,e)=>{
    for (let i = 0; i <  pickColor.length; i++)
         pickColor[i].classList.remove("active");
     pickColor[e].classList.add("active"),
    house.className = "house " + i
}
);
